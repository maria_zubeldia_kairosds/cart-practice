FROM node:16-alpine3.14 as builder
RUN mkdir /
WORKDIR /
COPY . .
RUN npm ci --legacy-peer-deps
RUN npm run build
RUN npm prune --production --legacy-peer-deps

FROM nginx:1.23.1-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /dist/ /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
