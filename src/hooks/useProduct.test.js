import { act, renderHook } from "@testing-library/react-hooks";
import { describe, expect, it, vi } from "vitest";
import { useProduct } from "./useProduct";

const responseGetProducts = [
	{
		brand: "Acer exe",
		id: "ZmGrkLRPXOTpxsU4jjAcv",
		imgUrl: "https://front-test-api.herokuapp.com/images/ZmGrkLRPXOTpxsU4jjAcv.jpg",
		model: "Iconia Talk S",
		price: "170"
	},
	{
		brand: "Acer exe",
		id: "cGjFJlmqNPIwU59AOcY8H",
		imgUrl: "https://front-test-api.herokuapp.com/images/cGjFJlmqNPIwU59AOcY8H.jpg",
		model: "Liquid Z6 Plus",
		price: "250"
	}
];

vi.mock("../repositories/product-repository.service", () => {

	return {
		getAllProductsRepository: vi.fn().mockImplementation(() => {
			return responseGetProducts;
		}),
	};
});

describe("useProduct custom hook", () => {


	it("it should render a product", async () => {
		const { result } = renderHook(() => useProduct());
		await act(() => {
			result.current.handleApiCall();
		});

		expect(result.current.productsData).toHaveLength(2);
		expect(result.current.productsData[0].brand).toBe("Acer exe");
		expect(result.current.productsData[0]["id"]).toBe("ZmGrkLRPXOTpxsU4jjAcv");
		expect(result.current.productsData[0]["model"]).toBe("Iconia Talk S");
		expect(result.current.productsData[1]["id"]).toBe("cGjFJlmqNPIwU59AOcY8H");
		expect(result.current.productsData[1]["price"]).toBe("250");
	});
});