import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { CartContext } from "../contexts/cart.context";
import { executeGetProductById } from "../usecases/get-product-by-id.usecase";
import { executePostProduct } from "../usecases/post-product.usecase";

const useProductDetail = () => {
	const [product, setProduct] = useState();
	const { productId } = useParams();
	const [idFormProduct, setIdFormProduct] = useState("");
	const [colorFormProduct, setColorFormProduct] = useState("");
	const [storageFormProduct, setStorageFormProduct] = useState("");

	const ctxCart = useContext(CartContext);

	useEffect(() => {
		getProductDetails(productId);
		setIdFormProduct(productId);

	}, [productId]);

	const getProductDetails = async (productId) => {
		const productFromApi = await executeGetProductById(productId);
		setProduct(productFromApi);
	};

	const handlePostApiCall = async (newProduct) => {
		try {
			await executePostProduct(newProduct);
		} catch (error) {
			console.log("error al añadir producto cesta", error);
		}
	};

	const handleNewSub = (newItem) => {
		ctxCart.setCart((cartItemData) => [...cartItemData, newItem]);
	};

	const handleAddToCart = (ev) => {
		ev.preventDefault();

		const newProduct = {
			id: idFormProduct,
			colorCode: colorFormProduct,
			storageCode: storageFormProduct
		};

		if ((newProduct.colorCode !== "") && (newProduct.storageCode !== "")) {
			handleNewSub(newProduct);
			handlePostApiCall(newProduct);
			alert("product added to the cart succesfully");
		} else {
			alert("select the options available");
		}
	};

	return {
		product,
		idFormProduct,
		setIdFormProduct,
		colorFormProduct,
		setColorFormProduct,
		storageFormProduct,
		setStorageFormProduct,
		handleAddToCart
	};
};

export default useProductDetail;