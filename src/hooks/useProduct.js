import { useEffect, useState } from "react";
import { executeGetAllProducts } from "../usecases/get-all-products.usecase";

export const useProduct = () => {
	const [productsData, setProductsData] = useState([]);
	const [searchedProducts, setSearchedProducts] = useState([]);
	const [inputValue, setInputValue] = useState("");


	useEffect(() => {
		handleApiCall();
	}, []);

	const setWithExpiryDate = (key, value, expiryTime) => {
		const now = new Date();

		const item = {
			value: value,
			expiry: now.getTime() + expiryTime
		};
		localStorage.setItem(key, JSON.stringify(item));
	};

	const getWithExpiryDate = (key) => {
		const itemStr = localStorage.getItem(key);

		if (!itemStr) {
			handleApiCall();
		}

		const item = JSON.parse(itemStr);
		const now = new Date();

		if (now.getTime() > item.expiry) {
			localStorage.removeItem(key);
			handleApiCall();
		}
		return item.value;
	};

	const handleApiCall = async () => {
		const localStorageTime = 3600000;
		//1h= 3600000
		//1m= 60000

		let localStorageKeys = Object.keys(localStorage);

		if (localStorage.length > 0 && localStorageKeys == "productsTime") {
			const localProducts = getWithExpiryDate("productsTime");
			setProductsData(localProducts);
			setSearchedProducts(localProducts);
		}
		else {
			const apiData = await executeGetAllProducts();
			setWithExpiryDate("productsTime", apiData, localStorageTime);
			setProductsData(apiData);
			setSearchedProducts(apiData);
		}
	};

	return {
		productsData,
		setProductsData,
		handleApiCall,
		inputValue,
		setInputValue,
		searchedProducts,
		setSearchedProducts,
	};
};