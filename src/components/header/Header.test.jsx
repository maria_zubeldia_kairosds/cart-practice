import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { describe, expect, it } from "vitest";
import Header from "./Header";

describe("header", () => {

	it("renders header properly", () => {

		const cartItems = [
			{
				"colorCode": "Black",
				"id": "xyPoqGJxYR4Nn3yVGQcfI",
				"storageCode": "16 GB"
			},
			{
				"colorCode": "Black",
				"id": "cGjFJlmqNPIwU59AOcY8H",
				"storageCode": "32 GB"
			}
		];
		const cartCounter = cartItems.length;

		render(<Header cartItems={cartItems} />, { wrapper: MemoryRouter });
		expect(cartCounter).toBeDefined();
		expect(cartCounter).toBe(2);
		expect(screen.getByRole("heading", {
			level: 1
		})
		).toHaveTextContent("Cart Practice");
	});
});