import styled from "styled-components";

export const HeaderContainer = styled.form`
    display: flex;
    height: 4.5rem;
    justify-content: space-between;
    align-items: center;
    padding: 1rem;
    background-color: var(--dark-bg-color);
    color: var(--light-bg-color);
`;

export const HeaderTitle = styled.h1`
    cursor: pointer;
    :hover{
        color: var(--highlight-color)
    }
`;