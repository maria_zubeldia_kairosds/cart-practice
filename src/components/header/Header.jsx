import React from "react";
import { useNavigate } from "react-router-dom";
import { HeaderContainer, HeaderTitle } from "./HeaderStyles";

const Header = ({ cartNum }) => {
	const navigate = useNavigate();

	return (
		<HeaderContainer>
			<HeaderTitle onClick={() => navigate("/")}>
				Cart Practice
			</HeaderTitle>
			<div>
				<span className="fa-solid fa-cart-shopping"></span>
				<span> {cartNum}</span>
			</div>
		</HeaderContainer>
	);
};

export default Header;