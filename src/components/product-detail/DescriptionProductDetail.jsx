import { BlockListWrapper, ElementListSectionDetail, ListWraperSectionDetail, SectionDetail, SpanElement, TitleSectionDetail } from "./DescriptionProductDetailStyles";

import React from "react";

const DescriptionProductDetail = ({ product }) => {
	return (

		<SectionDetail>
			<TitleSectionDetail id={product.id}>{product.model}</TitleSectionDetail>
			<ListWraperSectionDetail>

				<BlockListWrapper>
					<ElementListSectionDetail>
						<SpanElement>Brand</SpanElement>: {product.brand}
					</ElementListSectionDetail>
					<ElementListSectionDetail>
						<SpanElement>Price</SpanElement>: {product.price}$
					</ElementListSectionDetail>
					<ElementListSectionDetail>
						<SpanElement>Status</SpanElement>: {product.status}
					</ElementListSectionDetail>
					<ElementListSectionDetail>
						<SpanElement>CPU</SpanElement>: {product.cpu}
					</ElementListSectionDetail>
					<ElementListSectionDetail>
						<SpanElement>RAM</SpanElement>: {product.ram}
					</ElementListSectionDetail>
					<ElementListSectionDetail>
						<SpanElement>Operative System</SpanElement>: {product.os}
					</ElementListSectionDetail>
					<ElementListSectionDetail>
						<SpanElement>Screen Resolution</SpanElement>: {product.displaySize}
					</ElementListSectionDetail>
				</BlockListWrapper>

				<BlockListWrapper>
					<ol>
						<SpanElement>Cameras</SpanElement>:
						<ElementListSectionDetail>
						Main: {product.primaryCamera}
						</ElementListSectionDetail>
						<ElementListSectionDetail>
						Secondary: {product.secondaryCmera}
						</ElementListSectionDetail>
					</ol>
					<ElementListSectionDetail>
						<SpanElement>Battery</SpanElement>: {product.battery}
					</ElementListSectionDetail>
					<ElementListSectionDetail>
						<SpanElement>Dimentions</SpanElement>: {product?.dimentions}
					</ElementListSectionDetail>
					<ElementListSectionDetail>
						<SpanElement>Weight</SpanElement>: {product.weight}g
					</ElementListSectionDetail>
				</BlockListWrapper>

			</ListWraperSectionDetail>
		</SectionDetail >


	);
};

export default DescriptionProductDetail;