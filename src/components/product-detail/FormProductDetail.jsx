import { ButtonCart, FormContainerDetail, SelectDetail } from "./FormProductDetailStyles";

import React from "react";

const FormProductDetail = ({
	product,
	handleAddToCart,
	setColorFormProduct,
	setStorageFormProduct

}) => {
	return (
		<FormContainerDetail onSubmit={e => e.preventDefault()}>
			<div>
				<SelectDetail
					onChange={(ev) => setStorageFormProduct(ev.target.value)}
					name="storageCode"
					id="storageCode"
					required
				>
					<option >Storage</option >
					{product.options.storages.map(storage => {
						return (
							<option
								value={storage.name}
								key={storage.code}>
								{storage.name}
							</option>
						);
					})}
				</SelectDetail>

				<SelectDetail
					onChange={(ev) => setColorFormProduct(ev.target.value)}
					name="colorCode"
					id="colorCode"
					required
				>
					<option>Color</option>
					{product.options.colors.map(color => {
						return (
							<option
								value={color.name}
								key={color.code}>
								{color.name}
							</option>
						);
					})}

				</SelectDetail>
			</div>

			<ButtonCart onClick={handleAddToCart}>Add to cart</ButtonCart>
		</FormContainerDetail>
	);
};

export default FormProductDetail;