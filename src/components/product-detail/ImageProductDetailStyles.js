import styled from "styled-components";

export const ImageProductDetailElement = styled.img`
    padding: 1.5rem;
    margin: 1rem;
`;