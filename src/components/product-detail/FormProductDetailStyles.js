import styled from "styled-components";

export const FormContainerDetail = styled.form`
    display: flex;
    flex-direction: column;
    margin: 1rem;
    align-items: center;
`;

export const SelectDetail = styled.select`
    margin: 1rem 0.5rem;
    width: 5rem;
`;

export const ButtonCart = styled.button`
    margin-top: 2rem;
    border: 2px solid var(--standard-color);
    border-radius: 0.25em;
    height: 2.4rem;

    :hover{
        background-color: var(--highlight-color);
        border: 2px solid var(--highlight-color);
        color: var(--light-bg-color)

    }
`;