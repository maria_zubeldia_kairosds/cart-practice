import React from "react";
import { ImageProductDetailElement } from "./ImageProductDetailStyles";


const ImageProductDetail = ({product}) => {
	return (
		<>
			<ImageProductDetailElement src={product.imgUrl} alt={product.model}/>
		</>
	);
};

export default ImageProductDetail;