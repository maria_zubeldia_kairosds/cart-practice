import styled from "styled-components";

export const SectionDetail = styled.section`
    display: flex;
    flex-direction: column;
    margin: 1rem;
    align-items: center;
`;

export const TitleSectionDetail = styled.h2`
    color: var(--standard-color);
    font-size: 2rem;
    padding: 1rem;
`;

export const ListWraperSectionDetail = styled.ul`
    padding: 1rem;
    display: flex;
    flex-direction: column;
    align-items: center;
    @media (min-width: 760px) {
        flex-direction: row;
        justify-content: space-evenly;
    }
`;

export const BlockListWrapper = styled.div`
    margin-right: 0.5rem;
    width: 70%;
    @media (min-width: 760px) {
        width: 40%;
    }
`;

export const ElementListSectionDetail = styled.ul`
    padding: 0.4rem;
`;

export const SpanElement = styled.span`
    font-weight: 600;
`;