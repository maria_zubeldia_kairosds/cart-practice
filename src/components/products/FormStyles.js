import styled from "styled-components";

export const FormContainer = styled.form`
    
`;

export const InputElement = styled.input`
    height: 2.2rem;
    width: 12rem;
    padding-left: 0.5rem;

    :focus {
        outline: none;
        border:3px solid var(--highlight-color);
        box-shadow: 0 0 7px var(--dark-bg-color);
    }

    @media (min-width: 760px) {
        width: 17rem;
    }
`;

