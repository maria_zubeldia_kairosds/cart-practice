import React from "react";
import { useNavigate } from "react-router-dom";
import { ArticleWrapper, ListItem, ParagraphNotFound, UnorderedListContainer } from "./ProductListStyles";

const ProductListSearchedItem = ({ searchedProducts }) => {
	const navigate = useNavigate();

	return (
		<>
			{
				searchedProducts == "not found"
					? <ParagraphNotFound>Product not found, try a different one</ParagraphNotFound>
					: <UnorderedListContainer>
						{
							searchedProducts.map(searchedProduct => {
								return (

									<ListItem key={searchedProduct.id}>
										<ArticleWrapper className="article_wraper" onClick={() => navigate(`/product/${searchedProduct.id}`)}>
											<header>
												<h3>{searchedProduct.model}</h3>
											</header>
											<main>
												<img src={searchedProduct.imgUrl} />
												<p>Brand: {searchedProduct.brand}</p>
												<p>Price:
													{searchedProduct.price == ""
														? " Not available"
														: ` ${(searchedProduct.price)}$`
													}
												</p>
											</main>
										</ArticleWrapper>
									</ListItem>
								);
							})
						}
					</UnorderedListContainer>

			}
		</>
	);
};

export default ProductListSearchedItem;