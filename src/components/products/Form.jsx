import React from "react";
import { FormContainer, InputElement } from "./FormStyles";

const Form = ({ inputValue, setInputValue, setSearchedProducts, productsData }) => {

	const handleChange = (ev) => {
		setInputValue(ev.target.value);
		handleFilters(ev.target.value);
	};

	const handleFilters = (searchInput) => {
		let productNotFound = ["not found"];

		if (searchInput.length > 2) {
			const filteredData = productsData
				.filter((item) => {
					return Object.keys(item)
						.some((key) => {
							return item[key].toLowerCase().includes(searchInput.toLowerCase());
						});
				});

			if (filteredData.length == 0) {
				setSearchedProducts(productNotFound);
			} else {
				setSearchedProducts(filteredData);
			}
		} else if (searchInput.length == 0) {
			setSearchedProducts(productsData);
		}
	};

	return (
		<FormContainer onSubmit={e => e.preventDefault()}>
			<InputElement
				name="product"
				id="product"
				minLength={3}
				type="text"
				value={inputValue}
				onChange={handleChange}
				placeholder='Iconia'>
			</InputElement>
		</FormContainer>
	);
};

export default Form;