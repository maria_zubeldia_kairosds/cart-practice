import React from "react";
import ProductListSearchedItem from "./ProductListSearchedItem";

const ProductList = ({ searchedProducts }) => {
	return (
		<>
			<ProductListSearchedItem searchedProducts={searchedProducts} />
		</>
	);
};

export default ProductList;