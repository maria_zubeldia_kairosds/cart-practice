import styled from "styled-components";

export const UnorderedListContainer = styled.ul`
    padding: 1.5rem;
    display: grid;
    grid-template-columns: repeat(1, 1fr);
    gap: 20px;
    margin-left: auto;
    margin-right: auto;
    
    @media (min-width: 760px) {
    grid-template-columns: repeat(3, 1fr);
    width: 78%;
    }
    @media (min-width: 1200px) {
    grid-template-columns: repeat(4, 1fr);
    width: 90%;
    }
`;
export const ListItem = styled.li`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 0.75rem;
    height: 22rem;
`;
export const ArticleWrapper = styled.article`
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    text-align: center;
    border-bottom: solid 1px var(--standard-color);
    width: 75%;
    height: 100%;
    cursor: pointer;
    :hover{
        border-bottom: solid 2px var(--highlight-color);
    }
`;

export const ArticleHeader = styled.header`
    color: var(--standard-color);
`;

export const ParagraphNotFound = styled.h3`
    //text-align: justify;
`;