import { getAllProductsProxy, getProductByIdProxy, postProductProxy } from "../services/product-proxy.service";

export function getAllProductsRepository() {
	return getAllProductsProxy();
}
export function getProductByIdRepository(productId) {
	return getProductByIdProxy(productId);
}

export function postProductRepository(product) {
	return postProductProxy(product);
}