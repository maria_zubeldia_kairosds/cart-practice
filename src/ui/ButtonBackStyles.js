import styled from "styled-components";

export const IconBackDetail = styled.i`
    margin: 2rem 1.5rem;
    color: var(--standard-color);
    font-size: 2.3rem;
    cursor: pointer;
    :hover {
        color: var(--highlight-color)
    }
`;