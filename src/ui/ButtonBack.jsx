import React from "react";
import { useNavigate } from "react-router-dom";
import { IconBackDetail } from "./ButtonBackStyles";

const ButtonBack = () => {

	const navigate = useNavigate();

	return (
		<>
			<a onClick={() => navigate("/")}>
				<IconBackDetail className="fa-solid fa-circle-left"></IconBackDetail>
			</a>
		</>
	);
};

export default ButtonBack;