import axios from "axios";

const baseUrl = "https://front-test-api.herokuapp.com";

export async function getAllProductsProxy() {
	const response = await axios.get(`${baseUrl}/api/product`);
	return response.data;
}

export async function getProductByIdProxy(productId) {
	const response = await axios.get(`${baseUrl}/api/product/${productId}`);
	return response.data;
}

export async function postProductProxy(product) {
	const response = await axios.post(`${baseUrl}/api/cart`, product);
	return response.data;
}
