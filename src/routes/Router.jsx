import React, { useContext } from "react";
import { Route, Routes } from "react-router-dom";
import Header from "../components/header/Header";
import { CartContext } from "../contexts/cart.context";
import NotFound from "../pages/not-found/NotFound";
import ProductDetail from "../pages/product-detail/ProductDetail";
import Products from "../pages/product/Products";

const Router = () => {
	const ctxCart = useContext(CartContext);
	let cartNum = ctxCart?.cart.length;

	return (
		<>
			<Header cartNum={cartNum} />
			<Routes>
				<Route path="*" element={<NotFound />} />
				<Route path="/" element={<Products />}></Route>
				<Route path="/product/:productId" element={<ProductDetail />}></Route>
			</Routes>
		</>
	);
};

export default Router;