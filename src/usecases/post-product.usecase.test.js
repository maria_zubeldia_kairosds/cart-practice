import { afterEach, describe, expect, it, vi } from "vitest";

const responseGetProducts = [
	{
		brand: "Acer exe",
		id: "cGjFJlmqNPIwU59AOcY8H",
		imgUrl: "https://front-test-api.herokuapp.com/images/cGjFJlmqNPIwU59AOcY8H.jpg",
		model: "Liquid Z6 Plus",
		price: "250",
		status: "Available. Released 2016  December", 
		colors: ["Black", "White"], 
		options: { 
			"colors": [
				{ "code": 1000, "name": "Black" }, 
				{ "code": 1001, "name": "White" }
			], 
			"storages": [
				{ "code": 2000, "name": "32 GB" }]  
		}
	}
];

vi.mock("../repositories/product-repository.service", () => {

	return {
		postProductRepository: vi.fn().mockImplementation(() => {
			return responseGetProducts;
		}),
	};
});

import { executePostProduct } from "./post-product.usecase";

describe("Post one product use case", () => {
	afterEach(() => {
		vi.clearAllMocks();
	});
	it("should return all products", async () => {
		const newProduct = {
			id: "cGjFJlmqNPIwU59AOcY8H",
			colorCode: "Black",
			storageCode: "32 GB"
		};
		const useCase = await executePostProduct(newProduct);
		expect(useCase[0].colors[0]).toBe("Black");
		expect(useCase[0].id).toBe("cGjFJlmqNPIwU59AOcY8H");
		expect(useCase).toHaveLength(1);
	});
});