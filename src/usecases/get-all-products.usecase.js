import { getAllProductsRepository } from "../repositories/product-repository.service";

export function executeGetAllProducts() {
	return getAllProductsRepository();
}