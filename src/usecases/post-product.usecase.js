import { postProductRepository } from "../repositories/product-repository.service";

export function executePostProduct(product) {
	return postProductRepository(product);
}