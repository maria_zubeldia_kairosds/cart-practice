import { afterEach, describe, expect, it, vi } from "vitest";

const responseGetProducts = [
	{
		brand: "Acer exe",
		id: "ZmGrkLRPXOTpxsU4jjAcv",
		imgUrl: "https://front-test-api.herokuapp.com/images/ZmGrkLRPXOTpxsU4jjAcv.jpg",
		model: "Iconia Talk S",
		price: "170"
	}
];

vi.mock("../repositories/product-repository.service", () => {

	return {
		getProductByIdRepository: vi.fn().mockImplementation(() => {
			return responseGetProducts;
		}),
	};
});

import { executeGetProductById } from "./get-product-by-id.usecase";

describe("get product by id use case", () => {
	afterEach(() => {
		vi.clearAllMocks();
	});

	it("should return one product", async() => {
		const productId = "ZmGrkLRPXOTpxsU4jjAcv";
		const useCase = await executeGetProductById(productId);

		expect(useCase[0].id).toBe(productId);
		expect(useCase[0].model).toBe("Iconia Talk S");
		expect(useCase).toHaveLength(1);


	});
});