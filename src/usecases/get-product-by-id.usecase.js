import { getProductByIdRepository } from "../repositories/product-repository.service";

export function executeGetProductById(productId) {
	return getProductByIdRepository(productId);
}