import { afterEach, describe, expect, it, vi } from "vitest";

const responseGetProducts = [
	{
		brand: "Acer exe",
		id: "ZmGrkLRPXOTpxsU4jjAcv",
		imgUrl: "https://front-test-api.herokuapp.com/images/ZmGrkLRPXOTpxsU4jjAcv.jpg",
		model: "Iconia Talk S",
		price: "170"
	},
	{
		brand: "Acer exe",
		id: "cGjFJlmqNPIwU59AOcY8H",
		imgUrl: "https://front-test-api.herokuapp.com/images/cGjFJlmqNPIwU59AOcY8H.jpg",
		model: "Liquid Z6 Plus",
		price: "250"
	}
];


vi.mock("../repositories/product-repository.service", () => {

	return {
		getAllProductsRepository: vi.fn().mockImplementation(() => {
			return responseGetProducts;
		}),
	};
});

import { executeGetAllProducts } from "./get-all-products.usecase";

describe("Get all products use case", () => {
	afterEach(() => {
		vi.clearAllMocks();
	});
	it("should return all products", async () => {
		const useCase = await executeGetAllProducts();
		expect(useCase[0].brand).toBe("Acer exe");
		expect(useCase).toHaveLength(2);
	});
});
