import styled from "styled-components";

export const MainContainerNotFound = styled.main`
    display: flex;
    flex-direction: column;
    min-height: 30rem;
    justify-content: center;
    align-items: center;
`;

export const TitleNotFound = styled.h2`
    font-size: 2rem;
    padding: 1rem;
`;

export const LinkNotFound = styled.a`
    color: var(--standard-color);
    cursor: pointer;
    :hover{
        color: var(--highlight-color)
    }
`;