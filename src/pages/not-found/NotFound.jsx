import React from "react";
import { useNavigate } from "react-router-dom";
import ButtonBack from "../../ui/ButtonBack";
import { LinkNotFound, MainContainerNotFound, TitleNotFound } from "./NotFoundStyles";

const NotFound = () => {
	const navigate = useNavigate();
	return (
		<>
			<ButtonBack/>
			<MainContainerNotFound>
				<TitleNotFound>Something went wrong...</TitleNotFound>
				<p>Go back to the <LinkNotFound onClick={() => navigate("/")}>main page</LinkNotFound></p>
			</MainContainerNotFound>

		</>
	);
};

export default NotFound;