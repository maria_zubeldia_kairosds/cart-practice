import styled from "styled-components";

export const MainContainer = styled.main`
    padding: 1rem;
    display: flex;
    flex-direction: column;
`;

export const InputAndTitleWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 1rem;
    height: 7rem;
`;

export const ProductTitle = styled.h2`
    color: var(--dark-bg-color);
    font-size: 2rem;
`;