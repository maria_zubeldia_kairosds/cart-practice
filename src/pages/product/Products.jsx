import React from "react";
import Form from "../../components/products/Form";
import ProductList from "../../components/products/ProductList";
import { useProduct } from "../../hooks/useProduct";

import { InputAndTitleWrapper, MainContainer, ProductTitle } from "./ProductStyles";

const Products = () => {

	const {
		inputValue,
		setInputValue,
		searchedProducts,
		setSearchedProducts,
		handleApiCall,
		productsData,
	} = useProduct();

	return (
		<>
			<MainContainer>
				<InputAndTitleWrapper>
					<ProductTitle>Products</ProductTitle>
					<Form
						inputValue={inputValue}
						setInputValue={setInputValue}
						searchedProducts={searchedProducts}
						setSearchedProducts={setSearchedProducts}
						handleApiCall={handleApiCall}
						productsData={productsData}
					/>
				</InputAndTitleWrapper>
				<section>
					<ProductList searchedProducts={searchedProducts} />
				</section>
			</MainContainer>
		</>
	);
};

export default Products;