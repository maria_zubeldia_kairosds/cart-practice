import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { describe, expect, it } from "vitest";
import Products from "./Products";

describe("products", () => {
	it("renders products properly", () => {
		render(<Products />, { wrapper: MemoryRouter });
		expect(screen.getByRole("heading", {
			level: 2
		})
		).toHaveTextContent("Products");
	});

	it("renders its children", () => {
		const component = render(
			<Products>
				<div className='testDiv'>TestDivComponent</div>
			</Products>
			, { wrapper: MemoryRouter });

		expect(component.container.querySelector(".testDiv")).toBeDefined();
	});
});