import React from "react";
import DescriptionProductDetail from "../../components/product-detail/DescriptionProductDetail";
import FormProductDetail from "../../components/product-detail/FormProductDetail";
import ImageProductDetail from "../../components/product-detail/ImageProductDetail";
import useProductDetail from "../../hooks/useProductDetail";
import ButtonBack from "../../ui/ButtonBack";
import { MainContainerDetail } from "../product-detail/ProductDetailStyles";

const ProductDetail = () => {
	const {
		product,
		idFormProduct,
		setIdFormProduct,
		colorFormProduct,
		setColorFormProduct,
		storageFormProduct,
		setStorageFormProduct,
		handleAddToCart
	} = useProductDetail();

	return (
		<>
			<ButtonBack />
			<MainContainerDetail>
				{
					product
						?
						<>
							<div>
								<ImageProductDetail product={product} />
							</div>
							<div>
								<DescriptionProductDetail product={product} />
								<FormProductDetail
									idFormProduct={idFormProduct}
									setIdFormProduct={setIdFormProduct}
									colorFormProduct={colorFormProduct}
									setColorFormProduct={setColorFormProduct}
									storageFormProduct={storageFormProduct}
									setStorageFormProduct={setStorageFormProduct}
									product={product}
									handleAddToCart={handleAddToCart}
								/>
							</div>
						</>
						: (<p>Loading...</p>)
				}
			</MainContainerDetail>
		</>
	);
};

export default ProductDetail;