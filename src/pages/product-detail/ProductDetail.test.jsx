import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import { describe, it } from "vitest";
import ProductDetail from "./ProductDetail";

describe("product detail component", () => {
	it("renders product detail", () => {
		const component = render(<ProductDetail />, { wrapper: MemoryRouter });
		const li = component.container.querySelector("li");
		console.log(li);
	});
});