import styled from "styled-components";

export const MainContainerDetail = styled.main`
    display: flex;
    flex-direction: column;
    min-height: 30rem;
    justify-content: center;
    align-items: center;
    gap: 2rem;

    @media (min-width: 760px) {
    flex-direction: row;
    }
    @media (min-width: 1200px) {

    }
`;

