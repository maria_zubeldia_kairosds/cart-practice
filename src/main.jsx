import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { CartProvider } from "./contexts/cart.context";

import "./index.css";
import "./resets.css";

import Router from "./routes/Router";

ReactDOM.createRoot(document.getElementById("root")).render(
	<BrowserRouter>
		<CartProvider>
			<Router />
		</CartProvider>
	</BrowserRouter>
);
