/// <reference types="Cypress" />

describe("filling form tests", () => {

    beforeEach(() => {
        cy.intercept('POST', 'http://localhost:5173/')
    })

    it("should navigate to not found page", () => {
        cy.visit("http://localhost:5173/").wait(1500)
        cy.visit("http://localhost:5173/loremip").wait(1500)
        cy.title().should('include', 'Cart Practice')
        cy.get('.sc-eCYdqJ').contains("Something went wrong...").wait(1500)
        cy.get('.sc-jSMfEi').click()
        cy.get('.sc-kgflAQ').contains("Products")
    })

    it("should give result product not found", () => {
        cy.visit("http://localhost:5173/").wait(1500)
        cy.title().should('include', 'Cart Practice')
        cy.get('#product').type("dolorsit").wait(2000)
        cy.get('.sc-hHLeRK').contains("Product not found, try a different one").wait(2000)
        cy.get('#product').clear().type("iconia").wait(1500)
        cy.get(':nth-child(1) > .sc-idiyUo > header > h3').contains("Iconia")
        cy.get('#product').clear()
    })
})