/// <reference types="Cypress" />

describe("navigation from products to one product detail", () => {

    beforeEach(() => {
        cy.intercept('POST', 'http://localhost:5173/')
    })
    
    it("add one product to the cart", () => {
        cy.visit("http://localhost:5173/").wait(1000)
        cy.title().should('include', 'Cart Practice')
        cy.get('#product').clear()
        cy.get('#product').type("alcat").wait(1500)
        cy.get('.sc-idiyUo').click()
        cy.location('pathname').should('include', 'product')
        cy.get('#storageCode').select('32 GB')
        cy.get('#colorCode').select('Titanium Grey')
        cy.get('.sc-evZas').click()
        cy.on('window:alert', (text) => {
            expect(text).to.contain("product added to the cart succesfully")
        })
    })

    it("should trigger an error alert", () => {
        cy.get('.sc-gsnTZi').click()
        cy.title().should('include', 'Cart Practice')
        cy.get('#product').clear()
        cy.get('#product').type("preda").wait(2000)
        cy.get('.sc-bjUoiL').eq(0).click()
        cy.location('pathname').should('include', 'product')
        cy.get('#storageCode').select("64 GB")
        cy.get('.sc-evZas').click().wait(1500)
        cy.on('window:alert', (text) => {
            expect(text).to.contain("select the options available")
        })
        cy.get('.sc-dkzDqf').click()
        cy.title().should('include', 'Cart Practice')

    })

})
