<a name="readme-top"></a>
<div align="center">
  <a href="https://gitlab.com/maria_zubeldia_kairosds/cart-practice"></a>

<h3 align="center">Cart Practice</h3>
</div>


<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project


<p>This is a Single Page Application to manage a Phone Shop.</p>
<p align="center">
  <img src="https://gitlab.com/maria_zubeldia_kairosds/cart-practice/-/raw/master/public/preview_web.png">
</p>
<p>You can filter the phones either by model or brand.</p>
<p>To see the detail of the products, you can click them and add them into your shopping cart.</p>


### Built With


* React: [https://es.reactjs.org/]
* Vite: [https://vitejs.dev/guide/assets.html]



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

* Node
  ```sh
  npm install
  ```
* You can also pull the Docker image from the Docker Hub repository: [https://hub.docker.com/repository/docker/mzubeldia/cart-practice]

### Installation

1. Clone the repo either with SSH or HTTPS:
   ```sh
   git clone git@gitlab.com:maria_zubeldia_kairosds/cart-practice.git
   ```
   or
   
   ```sh
   git clone https://gitlab.com/maria_zubeldia_kairosds/cart-practice.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Start the project
   ```sh
   npm run start
   ```
3. Run the tests
   ```sh
   npm run test
   ```
4. Run the end to end tests
   ```sh
   npm run cy:open
   ```
5. Run the linter
   ```sh
   npm run lint
   ```
6. Run the production build
   ```sh
   npm run build
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
